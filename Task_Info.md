
## Database Tables

There are two tables in the database

1. genotypes table containing genotypes data (represented by file -genotypes.csv)
2. phenotypes table containing phenotypes data (represented by file -phenotypes.csv)

userid is the common field in the tables. The manual cleaning of the data was done for phenotypes files before generating final data.
The genotypes data and genotypes.csv files could not be uploaded because of the large size but the phenotypes data and phenotypes.csv 
files have been uploaded to the downloads section of the repository. 

---

## Code file

Code resides in the process_db python file in the master branch. The paths assume the folder structure that was present in the 
downloaded repository structure.

